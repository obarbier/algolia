FROM        golang
MAINTAINER  Olivier Barbier <obarbier@me.com>
RUN         go get bitbucket.org/obarbier/algolia
ENTRYPOINT  ["algolia"]