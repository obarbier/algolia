package main

import (
	"github.com/algolia/algoliasearch-client-go/algoliasearch"
	"fmt"
	"os"
)

const APPICATION_ID = "IHBLD8UWSF"
const API_KEY = "842659e82032f21d482ff204a9662b5a"

func main() {
	if len(os.Args[1:]) == 0 {
		printUsage()
		return
	}

	search_term := os.Args[1:][0]
	
	client := algoliasearch.NewClient(APPICATION_ID, API_KEY)

	index := client.InitIndex("cluster")

	params := algoliasearch.Map{
	  "attributesToRetrieve": []string{"id", "restaurant", "cluster"},
	  "hitsPerPage":          50,
	}

	res, err := index.Search(search_term, params)

	if err != nil {
		fmt.Println(err)
		return
	}

	for _, hit := range res.Hits {
		if str, ok := hit["id"].(float64); ok {
    		fmt.Println(hit["restaurant"], fmt.Sprintf("http://%s/restaurant/current/%d", hit["cluster"], int(str)))
		}
	}
}

func printUsage() {
	fmt.Println("Usage: algolia-search SEARCH_TERM")
}